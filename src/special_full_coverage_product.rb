require './src/full_coverage_product'
require 'set'

class SpecialFullCoverageProduct < FullCoverageProduct
  MULTIPLIER_BY_THREE_DAYS = Set[1,2,3,4]
  MULTIPLIER_BY_TWO_DAYS = Set[5,6,7,8,9]

  def can_be_sold?
    return false if @sellIn.zero? || @sellIn.negative?
    true
  end

  def update_price
    @sellIn -= STANDARD_DAYS_DECREASE
    if @sellIn.zero? || @sellIn.negative?
      @price = 0
    else
      case
      when MULTIPLIER_BY_THREE_DAYS.include?(@sellIn) then
        @price = @price.send(operator, factor_increase_decrease * 3)
      when MULTIPLIER_BY_TWO_DAYS.include?(@sellIn) then
        @price = @price.send(operator, factor_increase_decrease * 2)
      else
        @price = @price.send(operator, factor_increase_decrease)
      end
      @price = TOP_PRICE if @price >= TOP_PRICE
    end
  end
end
