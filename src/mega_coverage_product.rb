require './src/product'

class MegaCoverageProduct < Product
  TOP_PRICE = 80

  def can_be_sold?
    false
  end

  def sell
    return 'Can be sold' if can_be_sold?
    'Cannot be sold'
  end

  def update_price
    @price = TOP_PRICE
  end
end
