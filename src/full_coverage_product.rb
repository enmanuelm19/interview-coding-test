require './src/product'
class FullCoverageProduct < Product
  STANDARD_PRICE_INCREASE = 1

  protected

    def operator
      '+'
    end
end
