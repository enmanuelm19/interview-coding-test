require './src/product'

class SuperSaleProduct < Product
  FACTOR_INCREASE_DECREASE = 2

# def update_price
#   @sellIn -= STANDARD_DAYS_DECREASE
#   return if @price.zero?
#   if @sellIn.negative?
#     @price -= STANDARD_PRICE_DECREASE * 2
#   else
#     @price -= STANDARD_PRICE_DECREASE
#   end
#   @price = 0 if @price.negative?
# end

  protected

    def factor_increase_decrease
      FACTOR_INCREASE_DECREASE
    end
end
