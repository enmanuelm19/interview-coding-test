class Product
  attr_accessor :name, :sellIn, :price

  FACTOR_INCREASE_DECREASE = 1
  STANDARD_DAYS_DECREASE = 1
  TOP_PRICE = 50

  def initialize(name, sellIn, price)
    @name = name
    @sellIn = sellIn
    @price = price
  end

  def can_be_sold?
    true
  end

  def sell
    return 'Can be sold' if can_be_sold?
    'Cannot be sold'
  end

  def update_price
    @sellIn -= STANDARD_DAYS_DECREASE
    if @sellIn.negative?
      @price = @price.send(operator, factor_increase_decrease * 2)
    else
      @price = @price.send(operator, factor_increase_decrease)
    end
    @price = 0 if @price.negative?
    @price = TOP_PRICE if @price >= TOP_PRICE
  end

  protected

    def operator
      '-'
    end

    def factor_increase_decrease
      FACTOR_INCREASE_DECREASE
    end
end
