require './src/product'
require './src/full_coverage_product'
require './src/special_full_coverage_product'
require './src/mega_coverage_product'
require './src/super_sale_product'

products = []
products << Product.new('Medium Coverage', 10, 20)
products << FullCoverageProduct.new('Full Coverage', 2, 0)
products << Product.new('Low Coverage', 5, 7)
products << MegaCoverageProduct.new('Mega Coverage', 0, 80)
products << MegaCoverageProduct.new('Mega Coverage', -1, 80)
products << SpecialFullCoverageProduct.new('Special Full Coverage', 15, 20)
products << SpecialFullCoverageProduct.new('Special Full Coverage', 10, 49)
products << SpecialFullCoverageProduct.new('Special Full Coverage', 5, 49)
products << SuperSaleProduct.new('Super Sale', 3, 6)

puts 'OMGHAI!'

puts '------- day 0 -------'
puts 'name, sellIn, price'
products.each { |product| puts "#{product.name}, #{product.sellIn}, #{product.price}" }

(1..30).each do |day|
  puts ''
  puts "------- day #{day} -------"
  puts 'name, sellIn, price'
  products.map(&:update_price)
  products.each { |product| puts "#{product.name}, #{product.sellIn}, #{product.price}" }
end
