#### ComparaOnline interview solution

This is the solution of the compara online interview test, you can see it [here](https://github.com/comparaonline/interview-coding-test).

In order to run the required script do the following:

> docker build -t "test" .

To run all the tests:
> docker run test /bin/bash -c "rspec"

To run the example script:
> docker run test /bin/bash -c "ruby after_30_days.rb"

