require './src/special_full_coverage_product'

RSpec.describe SpecialFullCoverageProduct do
  context 'when are more than ten days left' do
    it 'should increase the price by one' do
      product = SpecialFullCoverageProduct.new('Special Full Coverage Product', 15, 20)
      product.update_price
      expect(product.price).to eq(21)
      expect(product.sellIn).to eq(14)
    end

    it 'should be able to sell' do
      product = SpecialFullCoverageProduct.new('Special Full Coverage Product', 15, 20)
      product.update_price
      expect(product.can_be_sold?).to eq(true)
    end
  end

  context 'when are less than ten days left' do
    it 'should the price increase by two' do
      product = SpecialFullCoverageProduct.new('Special Full Coverage Product', 10, 20)
      product.update_price
      expect(product.price).to eq(22)
      expect(product.sellIn).to eq(9)
    end
  end

  context 'when are less than 5 days left' do
    it 'should the price increase by three' do
      product = SpecialFullCoverageProduct.new('Special Full Coverage Product', 5, 20)
      product.update_price
      expect(product.price).to eq(23)
      expect(product.sellIn).to eq(4)
    end
  end

  context 'when are not more days left' do
    it 'should the price drops to zero' do
      product = SpecialFullCoverageProduct.new('Special Full Coverage Product', 1, 20)
      product.update_price
      expect(product.price).to eq(0)
      expect(product.sellIn).to eq(0)
    end

    it 'should not be sold' do
      product = SpecialFullCoverageProduct.new('Speciall Full Coverage Product', 1, 20)
      product.update_price
      expect(product.can_be_sold?).to eq(false)
      expect(product.sell).to eq('Cannot be sold')
    end
  end
end
