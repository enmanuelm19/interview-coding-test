require './src/full_coverage_product'

RSpec.describe FullCoverageProduct do
  context 'when a single day has passed' do
    it 'should increase price by one' do
      product = FullCoverageProduct.new('Full Coverage', 2, 0)
      product.update_price
      expect(product.price).to eq(1)
      expect(product.sellIn).to eq(1)
    end
  end

  context 'when price reach top price and are days left' do
    it 'should remain at top price' do
      product = FullCoverageProduct.new('Full Coverage', 10, 50)
      (0..5).each { |days| product.update_price }
      expect(product.price).to eq(50)
      expect(product.sellIn).to eq(4)
    end
  end
end
