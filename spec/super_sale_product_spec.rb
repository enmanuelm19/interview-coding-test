require './src/super_sale_product'

RSpec.describe SuperSaleProduct do
  context 'when a single day has passed' do
    it 'should decrease the price by two' do
      product = SuperSaleProduct.new('Super Sale', 5, 7)
      product.update_price
      expect(product.price).to eq(5)
      expect(product.sellIn).to eq(4)
    end
  end

  context 'when days left to sell past' do
    it 'should price decrease twice as fast' do
      product = SuperSaleProduct.new('Super Sale', 5, 50)
      (0..5).each { |day| product.update_price }
      expect(product.price).to eq(36)
    end
  end
end
