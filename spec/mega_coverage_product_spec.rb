require './src/mega_coverage_product'

RSpec.describe MegaCoverageProduct do
  context 'when a single day has passed' do
    it 'should stay the price' do
      product = MegaCoverageProduct.new('Mega Coverage', 0, 80)
      product.update_price
      expect(product.price).to eq(80)
    end
  end

  context 'when it wants to be sold' do
    it 'should not be sold' do
      product = MegaCoverageProduct.new('Mega Coverage', 0, 80)
      expect(product.can_be_sold?).to be(false)
      expect(product.sell).to eq('Cannot be sold')
    end
  end
end
