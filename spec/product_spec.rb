require './src/product'

RSpec.describe Product do
  context 'when a single day has passed' do
    it 'should decrease price by one' do
      product = Product.new('Low Coverage', 5, 7)
      product.update_price
      expect(product.price).to eq(6)
      expect(product.sellIn).to eq(4)
    end

    it 'should can be sold' do
      product = Product.new('Low Coverage', 5, 7)
      product.update_price
      expect(product.can_be_sold?).to eq(true)
    end
  end

  context 'when days left to sell past' do
    it 'should price decrease twice as fast' do
      product = Product.new('Low Coverage', 5, 50)
      (0..5).each { |day| product.update_price }
      expect(product.price).to eq(43)
      expect(product.sellIn).to eq(-1)
    end
  end

  context 'when days left to sell past and the price is reach zero' do
    it 'should stay the price at zero' do
      product = Product.new('Low Coverage', 5, 6)
      (0..5).each { |day| product.update_price }
      expect(product.price).to eq(0)
      expect(product.sellIn).to eq(-1)
    end
  end
end
