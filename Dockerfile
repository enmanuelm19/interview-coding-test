FROM ruby:2.5.6
RUN mkdir -p /interview-coding-test
WORKDIR ./interview-coding-test
COPY . ./
RUN gem install bundler && bundle
ENTRYPOINT ["bundle", "exec"]
